# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
# See https://github.com/Rangi42/polished-map/blob/63c033/INSTALL.md
# Seems that polished-map needs very specific software versions
pkgname=polished-map-plusplus
pkgver=2.7.1+git20230130.ae8cce
pkgrel=0
_pkgname="polished-map"
_commit="ae8ccee81a7803b710213df3a24235545d6dd740"
pkgdesc='Map and tileset editor for projects that use 256-512 tiles and per-block attributes (Polished Crystal v3, Red++ v4, Coral, Black and White 3: Genesis, Ancient Ruby and pokecrystal projects that have added attributes.bin files)'
arch=("amd64")
license=("LGPL-3.0")
url="https://github.com/Rangi42/polished-map/tree/plusplus"
makedepends=("autoconf" "cmake" "libpng-dev" "libxpm-dev" "zlib1g-dev" "libx11-dev" "libxft-dev" "libxinerama-dev" "libfontconfig1-dev" "x11proto-xext-dev" "libxrender-dev" "libxfixes-dev" "libgl1-mesa-dev" "libglu1-mesa-dev" "libpulse-dev")
depends=("zlib1g" "libxpm4" "libpng16-16" "libx11-6" "libxft2" "libxinerama1" "libfontconfig1" "libxrender1" "libxfixes3" "libpulse0")
source=(
	"0001-polished-map-plusplus-usr-prefix.patch"
	"0002-correct-desktop-file.patch"	
	"$pkgname-$pkgver.tar.gz::https://github.com/Rangi42/polished-map/archive/ae8cce.tar.gz"
	"fltk_1.3.7.tar.gz::https://github.com/fltk/fltk/archive/refs/tags/release-1.3.7.tar.gz"
)
sha256sums=('074e46ef4fe3d50babd93dc0c83078e8f4ab4615837c823eaa0b1e3e331aaa19'
            'd3f7bf23a2ab703211ae3d48360a7440a33c30aa4679838577539ef064c68e26'
            '139fadf8e4bb5bfd8e9b0a696ec7f9fcbce5783c9596698d5638bc3f11725a0b'
            '019f65810fb0ea5acac14c852193e8f374e822e6a3034a3c80ed8676f6f3a090')

prepare() {
	cd $_pkgname-$_commit

	# Apply patches
	yes | patch -p1 < $srcdir/0001-polished-map-plusplus-usr-prefix.patch
	yes | patch -p1 < $srcdir/0002-correct-desktop-file.patch
}

build() {
	cd $srcdir
	# Build dependencies
	# FLTK
	cd fltk-release-1.3.7
	./autogen.sh --prefix="$PWD/.." --with-abiversion=10307
	make
	make install

	# Build main package
	# ("export PATH" is needed if bin/fltk-config is not already in your PATH)
	cd $srcdir/$_pkgname-$_commit
	export PATH="../bin:$PATH"
	make
}

package() {
	cd $_pkgname-$_commit
	make DESTDIR="$pkgdir" install
}


# vim: set ts=2 sw=2 et:
